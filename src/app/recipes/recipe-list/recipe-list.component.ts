import { Component, OnInit } from '@angular/core';
import {Recipe} from "../recipe.model";

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
recipes:Recipe[] = [
  new Recipe('Test recipe','Im just testing','https://www.shutterstock.com/image-photo/chef-slicing-vegetables-1211959120'),
  new Recipe('Test recipe','Im just testing','https://www.shutterstock.com/image-photo/chef-slicing-vegetables-1211959120')
];
  constructor() { }

  ngOnInit(): void {
  }


}
