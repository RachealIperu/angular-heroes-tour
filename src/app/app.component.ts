import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Tour of heroes';
  evenNums:number[]=[];
  oddNums:number[]=[];
  onIntervalFired(lastNumber:number){
    console.log(lastNumber);
    if(lastNumber %2 ===0){
      this.evenNums.push(lastNumber)
    }else {
      this.oddNums.push(lastNumber)
    }
  }



}
