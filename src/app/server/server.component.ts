import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-server',
  templateUrl: './server.component.html',
  styleUrls: ['./server.component.css']
})
export class ServerComponent implements OnInit {
  username = "Iperu";
  para = false;
  log: Date[]= [];

  constructor() { }

  ngOnInit(): void {
  }
  notNull(){
    this.username = '';
  }

  toggling(){
    this.para = !this.para;
    // this.log.push(this.log.length + 1);
    this.log.push(new Date());
  }

}
